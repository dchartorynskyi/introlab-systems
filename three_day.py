import datetime as dt
import pandas as pd


def three_day_before_change(file_name):
    fields = ['Date', 'Open', 'High', 'Low', 'Close', 'Volume']

    df = pd.read_csv(file_name)
    df["3day_before_change"] = ""
    df.to_csv(file_name, index=False)

    df = pd.read_csv(file_name, skipinitialspace=True, usecols=fields)

    first_column = df.iloc[:, 0]
    close_column = df.iloc[:, 4]

    p_1_index = 0
    p_2_index = 1

    result = ['' for x in range(len(first_column))]

    while p_2_index < len(first_column):
        p_1 = first_column[p_1_index]
        p_2 = first_column[p_2_index]
        if dt.datetime.strptime(p_2, '%Y-%m-%d') - dt.datetime.strptime(p_1, '%Y-%m-%d') == dt.timedelta(days=3):
            result[p_1_index] = float(close_column[p_2_index]) / float(close_column[p_1_index])
            p_1_index += 1
            p_2_index += 1
        elif dt.datetime.strptime(p_2, '%Y-%m-%d') - dt.datetime.strptime(p_1, '%Y-%m-%d') > dt.timedelta(days=3):
            p_1_index += 1
        elif dt.datetime.strptime(p_2, '%Y-%m-%d') - dt.datetime.strptime(p_1, '%Y-%m-%d') < dt.timedelta(days=3):
            p_2_index += 1

    df['3day_before_change'] = result
    df.to_csv(file_name)
