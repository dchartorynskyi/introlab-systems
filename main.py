import os
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

from scrap import get_news
from three_day import three_day_before_change


dir_path = os.path.dirname(os.path.realpath(__file__))
company_list = ['PD', 'ZUO', 'PINS', 'ZM', 'PCTL', 'DOCU', 'CLDR', 'RUN']

options = webdriver.ChromeOptions()

prefs = {'download.default_directory': dir_path}

options.add_experimental_option('prefs', prefs)

driver = webdriver.Chrome(chrome_options=options)

wait = WebDriverWait(driver, 10)
driver.get("https://finance.yahoo.com/")


def get_company(name):

    element = driver.find_element_by_id("yfin-usr-qry")
    element.send_keys(name)
    wait.until(lambda driver: driver.find_element_by_xpath("//div[@class='modules_resultsContainer__3P2fJ modules_BoxShadow__2Fw77']//li[@class='modules_linkItem__2NK9M modules_selectedBackground__3xdSN']")).click()
    hist_data = wait.until(lambda driver: driver.find_element_by_link_text('Historical Data'))
    hist_data.click()
    date = wait.until(lambda driver: driver.find_element_by_xpath("//span[@class='C($linkColor) Fz(14px)']"))
    date.click()
    button_max = driver.find_element_by_xpath("//ul[2]//li[4]//button[1]")
    button_max.click()
    button_apply = driver.find_element_by_xpath("//button[contains(@class,'Py(9px) Fl(end)')]")
    button_apply.click()
    download_button = driver.find_element_by_xpath("//span[contains(text(),'Download')]")
    download_button.click()


if __name__ == '__main__':
    for company_name in company_list:
        get_company(company_name)
        while not os.path.exists(f'{dir_path}/{company_name}.csv'):
            print(os.path.exists(dir_path + '/' + company_name))
            time.sleep(1)
        three_day_before_change(company_name + '.csv')
        get_news(company_name)
    driver.close()