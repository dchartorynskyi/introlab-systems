import requests
from bs4 import BeautifulSoup
import csv


def get_news(company_name):
    company_url = f'https://finance.yahoo.com/quote/{company_name}?p={company_name}&.tsrc=fin-srch'
    main_url = 'https://finance.yahoo.com/'

    html = requests.get(company_url).text

    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', id='quoteNewsStream-0-Stream').find('ul', class_='My(0) Ov(h) P(0) Wow(bw)').find_all('li', class_='js-stream-content Pos(r)')

    with open(company_name + 'news.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow(['title', 'url'])
        for tag in ads:
            try:
                title = tag.find('h3', class_='Mb(5px)').text.strip()
            except:
                title = ''

            try:
                url = tag.find('h3', class_='Mb(5px)').find('a').get('href')
            except:
                url = ''

            data = {'title': title, 'url': main_url + url}

            writer.writerow((data['title'], data['url']))
